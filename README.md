# things
A collection of little 3D printable things. Each small enough to not put it in a separate repository
and to not provide proper documentation. :)

All are designed in [OpenSCAD](https://www.openscad.org/) or the Python OpenSCAD wrapper
[SolidPython](https://github.com/SolidCode/SolidPython). All are more or less parameterizable.


### Battery Rack
A battery stand which I use for 18650 batteries and other formats.

Configurable for different battery sizes, depth, the number of battery slots in width and height, and wall thickness.

* Language: SolidPython
* Slicing/Printing Notes:
  * Can be printed pretty coarse.
  * Line width: A factor of the wall thickness (0.8 mm by default). So, use 0.4 or 0.2. 0.4 is totally OK.
  * No infill needed.
  * Avoid stringing and an over-flow on the inside. Artifact inside the slots might damage the batteries isolation. 


### Bed Level
Yet another bed leveling pattern, optimized for 0.2 mm nozzles.

### BMW Bike Charging Pole Cover
A little plastic cover for my bike's battery charging connector.

### Cherry MX Stamp
An OpenSCAD “cutting die” for Cherry MX switches.
To be used as subtrahend in other OpenSCAD models' ``difference`` operations.

### Ironing Board Cable Clip
A Clip for my iron's cable, mounted on my ironing board.

Works for a cable diameter of ~7.5 mm and a board cable lever with 2.75. Both are parameterizable.

* Language: OpenSCAD
* Slicing/Printing Notes:
  * Print it fine (at least 0.2×0.2) and strong.
  * A soft plastic like PLA is preferable.
  * Try printing it laying on the short side with support.
  
### Keycaps
A SolidPython key-cap library and some key-caps.

### Toothbrush Cover
A cover for the brush part of toothbrushes and the most simple model ever. Consist of four cubes. :)
All dimensions are parameterizable. Ready-to-use parametrizations for Elmex and Meridol brushes.

* Language: OpenSCAD
* Slicing/Printing Notes:
  * Print it standing upright on the closed front surface.

### Watering Can Spout
An extension for watering cans, to make a big outlet much smaller.
By default, it fits on cans with a conical outlet with an average diameter of ~ 22.2 mm.
Parameterizable to fit other cans.
