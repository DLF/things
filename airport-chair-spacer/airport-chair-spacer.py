from solid import *
from solid.utils import *
from math import atan, pi

angle = atan(1.5 / 25) * 180 / pi
print(angle)

scad_render_to_file(
    difference()(
        cylinder(d1=26, d2=25, h=14, segments=100),
        down(1)(cylinder(d=6.9, h=20, segments=100)),
        color("yellow")(up(12)(back(15)(
            rotate(angle, [1, 0, 0])(
                left(15)(cube([30, 30, 10])),
            )
        )))
    ),
    "chair-spacer.scad"
)