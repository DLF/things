from batteryrack import rack
from solid import scad_render_to_file

scad_render_to_file(
    rack(
        depth=55,
        diameter=19.4,
        wall=0.8,
        x=4,
        y=3
    )
    ,  # - cube([22,22,60], center=True),
    'batrack_18650_4x3.scad'
)
