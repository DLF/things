from solid import *
from solid.utils import *


def rack_element(
        depth,
        diameter,
        wall
):
    width = 2 * wall + diameter
    return \
        cube([width, width, depth + wall]) \
        - \
        color(Magenta)(
            up(wall)(
                translate([wall+diameter/2, wall+diameter/2, 0])(
                    cylinder(d=diameter, h=depth+1, segments=90)
                )
                +
                translate([wall, wall+diameter/2, 0])(
                    cube([diameter, diameter/2, depth+1])
                )
            )
        )


def rack_layer(
        depth,
        diameter,
        wall,
        x,
):
    return sum([
        left(i*(diameter+wall))(
            rack_element(depth, diameter, wall)
        ) for i in range(x)
    ])


def rack(
        depth,
        diameter,
        wall,
        x,
        y
):
    print("Width: {}  Height: {}".format(
        wall + x * (wall + diameter),
        wall + y * (wall + diameter)
    ))
    return sum([
        back(i*(diameter+wall))(
            rack_layer(depth, diameter, wall, x)
        ) for i in range(y)
    ])


if __name__ == "__main__":
    scad_render_to_file(
        rack(
            depth=15,
            diameter=19,
            wall=0.8,
            x=1,
            y=1
        )
        ,#- cube([22,22,60], center=True),
        'libbatterierack.scad'
    )
    # scad_render_to_file(rack(
    #     depth=54,
    #     diameter=18.4,
    #     wall=0.8,
    #     x=4,
    #     y=3
    # ), 'libbatterierack.scad')
