line_width= 0.2;
height = line_width;

module rect(length, thickness) {
    difference(){
        cube([length, length, height], center=true);
        cube([length - 2*thickness, length - 2*thickness, height + 2], center=true);
    }
}

module frect(length) {
    cube([length, length, height], center= true);
}

rect(175, 3*line_width);
rect(170, 3*line_width);
translate([75,75]) frect(15);
translate([-75,75]) frect(15);
translate([75,-75]) frect(15);
translate([-75,-75]) frect(15);
