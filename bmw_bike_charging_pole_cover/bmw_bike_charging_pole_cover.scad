wall = 1.6;
c_d_1 = 15;
c_d_2 = 14.4;
c_h = 10;
ring_profile_r = 0.4;
ring_r = c_d_2/2 + 0.2;

// 0.4 - 0.2 = 0.1

module outer() {
    cylinder(h=c_h + wall, d1=c_d_1 + 2*wall, d2=c_d_2 + 2*wall, $fn=20);
}

module inner() {
    translate([0,0,-0.01]) cylinder(h=c_h, d1=c_d_1, d2=c_d_2, $fn=50);
}

module ring() {
    translate([0, 0, c_h - 3])
        rotate_extrude($fn=100)
            translate([ring_r,0,0])
                circle(r=ring_profile_r, $fn=100);
}

intersection() {
    ring();
    outer();
}

difference() {
    outer();
    inner();
}
