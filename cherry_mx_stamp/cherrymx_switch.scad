
module switch_hole() {
    // “Negative” hole for MX Swiches. Use it as as
    // subtrahend in a difference operation.
    //
    // The hole at z=0 is the level on which the MX
    // rim hits the plate. The hole can be further
    // lowered, a bigger hole having space for the
    // rim is above z=0. The hole can easily be lowered
    // by 0.5mm.
    //
    // The hole stencil is centered on the xy-plane.
    //
    // The inner hole is 14mm^2.
    // The basic cutaway around is 16mm^2.
    // The gap in the MX rim is 11mm long
    //
    // BTW: The actual width of the switch (at rim) is likely specified as 15.6mm.
    //
    //

    switch_x = 14;
    switch_y = 14;

    switch_hat_x = 16.4;
    switch_hat_y = 16.4;

    clip_height = 0.8;
    clip_depth = 1;
    clip_width = 5;

    hat_cutaway_width = 10;

    total_height = 24;
    top_height = 10;

    lower_cutaway_height = total_height - top_height - clip_height;



    translate([0, 0, -total_height/2 + top_height]) rotate(90) {
    // rotate around z-axis so that the base positon is like looking from the front of the keyboard
        color("red") cube([switch_x, switch_y, total_height], center=true);
        translate([0,0,0]) {
            color("yellow") {
                // lower cut-away
                translate([0, 0, -(total_height-lower_cutaway_height)/2]) cube([
                    switch_y + 2*clip_depth,
                    clip_width,
                    lower_cutaway_height],
                    center=true
                );
                // upper cut-away
                translate([0, 0, (total_height-top_height)/2]) difference () {
                    cube([switch_hat_x, switch_hat_y, top_height], center=true);
                    translate([0, 0, -0.5]) cube([
                        hat_cutaway_width,
                        switch_hat_y + 1,
                        top_height],
                        center=true
                    );                
                }

            }
        }   
    }
}

switch_hole();
