center_radius = 40;
guard_angle=30;
cable_radius = 3.75;
clip_wall = 1.6;
cutout_width = 6;

socket_upper_width = 4;
socket_lower_width = 2;
socket_height = 9;

bolt_radius= 2.75;
bolt_hole_radius = 1.5;
bolt_length = 16;
bolt_offset = 0.1;

$fn=50;

module cross_section() {
    rotate([0, 0, 270])
    difference() {
        union() {
            circle(cable_radius + clip_wall);
            polygon([
                [-socket_upper_width/2,0],
                [-socket_lower_width/2, socket_height],
                [socket_lower_width/2, socket_height],
                [socket_upper_width/2,0]
            ]);
        }
        circle(cable_radius);
        translate([-cutout_width/2, -2*cable_radius])
            square([cutout_width, 2*cable_radius]);
    }
}

module mount_bolt(){
    translate([bolt_length+bolt_offset,0,0])
        rotate([0,270,0])
            cylinder(bolt_length, bolt_radius, bolt_radius);
}

module mount_bolt_hole(){
    translate([bolt_length+bolt_offset,0,0])
        rotate([0,270,0])
            translate([0,0,-0.01])
                color("red")
                    cylinder(bolt_length, bolt_hole_radius, bolt_hole_radius);
}

difference(){
union() {
    translate([center_radius-cable_radius,0,0])
        rotate([0,0,-guard_angle/2])
            rotate_extrude(angle=guard_angle, convexity=100, $fn=200)
                translate([-center_radius, 0])
                    cross_section();
    mount_bolt();
}
mount_bolt_hole();
}
