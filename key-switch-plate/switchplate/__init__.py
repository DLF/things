from solid import *

class SwitchPlate(object):

    def __init__(self, num_x, num_y, screw_length, stamp):
        self.num_x = num_x
        self.num_y = num_y
        self.screw_length = screw_length
        self.stamp = stamp
        self.switch_width = 19

