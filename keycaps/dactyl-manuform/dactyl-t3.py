from solid import *
from solid.utils import *
from keycab import shells, interfaces, Cap


class DactylThumb3(shells.MultiBase):

    def __init__(self):
        super(DactylThumb3, self).__init__(
            wall=1,
            height=10,
            shrink_factor=0.7
        )

    @property
    def base_form(self):
        p = polygon([
            (-9, -10),
            (9, -10),
            (12, 13),
            (-9, 14),
        ])
        c = left(0.9)(back(0.9)(circle(r=0.2, segments=20)))
        return minkowski()(p, c)
        return (p+c)

    @property
    def base_length(self):
        return 10 + 14

    @property
    def slant_body(self):
        return color("yellow")(
            rotate(90, [1, 0, 0])(
                cylinder(d1=20, d2=24, h=25)
            )
        )


dt3 = DactylThumb3()
scad_render_to_file(dt3() + dt3.slant_body, "dt3.scad")
