from solid.utils import *
from solid import scad_render_to_file
from keycab import shells, interfaces, Cap

dactyl_finger = Cap(
    shell=shells.Square(
        height=7,
        shrink_factor=0.75,
        corner_r=3.5,
        indent=0.8,
        squeeze=0.2,
        indent_radius=6.5
    ),
    interface=interfaces.BoxInterface()
)

dactyl_thumb_1_2 = Cap(
    shell=shells.Square(
        height=7,
        length=28,
        shrink_factor=0.75,
        corner_r=3.5,
        indent=0.8,
        squeeze=0.1,
        indent_radius=8.5
    ),
    interface=interfaces.BoxInterface()
)

dactyl_thumb_3 = Cap(
    shell=shells.Square(
        height=7,
        length=22,
        y_offset=1.9,
        shrink_factor=0.75,
        corner_r=3.5,
        indent=0.8,
        squeeze=0.1,
        indent_radius=8.5
    ),
    interface=interfaces.BoxInterface()
)

dactyl_thumb_5 = Cap(
    shell=shells.Square(
        height=7,
        shrink_factor=0.75,
        corner_r=3.5,
        indent=0.8,
        squeeze=0.2,
        indent_radius=6.5,
        tilt=13
    ),
    interface=interfaces.BoxInterface()
)

dactyl_finger_north = Cap(
    shell=shells.Square(
        height=8.5,
        top_height=1.5,
        shrink_factor=0.75,
        corner_r=3.5,
        indent=0.8,
        squeeze=0.2,
        indent_radius=6.5,
        tilt=8
    ),
    interface=interfaces.BoxInterface(
        # height=4.7
    )
)

scad_render_to_file(dactyl_finger(), "dactyl-finger-center.scad")
scad_render_to_file(dactyl_thumb_1_2(), "dactyl-thumb-1-2.scad")
scad_render_to_file(dactyl_thumb_3(), "dactyl-thumb-3.scad")
scad_render_to_file(dactyl_thumb_5(), "dactyl-thumb-5.scad")
scad_render_to_file(dactyl_finger_north(), "dactyl-finger-north.scad")

