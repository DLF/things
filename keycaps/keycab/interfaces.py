from solid import *
from solid.utils import *


class MXInterface(object):
    """
    Fits to original Cherry MX and Gateron.
    """
    def __init__(self, height=4):
        """
        :param height: The height of the pole in mm. Default is 4.
        """
        self.height = height

    def post(self):
        """
        Middle post width the cross adapter.
        :return: The pole.
        """
        height = self.height
        x1 = 1.3
        y1 = 4.2
        x2 = 4.2
        y2 = 1.4
        x_bar = 6
        y_bar = 3.2
        bar_rounding = 0.25

        def c(angle):
            return rotate((0, 0, angle))(
                translate((0.07, -0.07, 0))(
                    circle(d=0.2, segments=20)
                )
            )
        cross = up(0)(
            translate((0, 0, height / 2))(
                square((x1, y1), center=True)
                +
                square((x2, y2), center=True)
            )
            +
            left(x1 / 2)(
                forward(y1 / 2)(c(0))
                +
                back(y1 / 2)(c(90))
            )
            +
            right(x1 / 2)(
                forward(y1 / 2)(c(270))
                +
                back(y1 / 2)(c(180))
            )
            +
            left(x2 / 2)(
                forward(y2 / 2)(c(0))
                +
                back(y2 / 2)(c(90))
            )
            +
            right(x2 / 2)(
                forward(y2 / 2)(c(270))
                +
                back(y2 / 2)(c(180))
            )
        )

        return translate((0, 0, -height))(
            linear_extrude(height)(
                circle(r=2.75, segments=120)
                +
                translate((0, 0, height/2))(
                    translate((-(x_bar-2*bar_rounding)/2, -(y_bar-2*bar_rounding)/2, 0))(
                        minkowski()(
                            square((x_bar - 2*bar_rounding, y_bar - 2*bar_rounding)),
                            circle(r=bar_rounding, segments=30)
                        )
                    )
                )
                -
                cross
            )
            -
            down(1)(linear_extrude(1.2)(
                offset(r=0.2)(cross)
            ))
        )

    @staticmethod
    def cutaway():
        base_a = 18.75
        return translate((0, 0, -10))(
            color("yellow")(
                translate((0, 0, -0.01))(
                    linear_extrude(height=10, scale=0.6)(
                        square([base_a, base_a], center=True)
                    )
                )
            )
        )


class BoxInterface(object):
    def __init__(self, height=3.2):
        self.height = height

    @staticmethod
    def cutaway():
        return MXInterface.cutaway()

    def post(self):
        """
        Box inner edge: 6mm
        Box corner radius: 1.5mm (so there remain 3mm of straight edge
        Cross y: 1.1mm (per spec)
        Cross x: 1.32 mm (per spec)
        Both cross parts are 4mm long (per spec)
        :return: The pole.
        """
        return down(self.height)(
            linear_extrude(height=self.height)(
                difference()(
                    translate([-3, -3])(
                        minkowski()(
                            square([3, 3]),
                            translate([1.5, 1.5])(
                                circle(r=1.5, segments=50)
                            )
                        )
                    ),
                    square([1.2, 4], center=True),
                    square([4, 1.4], center=True)
                )
            )
        )


if __name__ == "__main__":
    i = BoxInterface()
    scad_render_to_file(i.post(), "test.scad")