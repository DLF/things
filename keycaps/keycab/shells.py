from solid import *
from solid.utils import *
import math


def deg_tan(v):
    return math.tan(v * math.pi / 180)


class MultiBase(object):

    def __init__(
        self,
        height=5,
        tilt=0,
        shrink_factor=0.8,
        wall=1,
        indent=0.8,
        x_offset=0,
        y_offset=0,
    ):
        self.height = height
        self.shrink_factor = shrink_factor
        self.tilt = tilt
        self.wall = wall
        self.indent = indent
        self.x_offset = x_offset
        self.y_offset = y_offset

    @property
    def tilt_height(self):
        return deg_tan(self.tilt) * self.base_length

    @property
    def total_height(self):
        return self.height + self.tilt_height + self.indent

    def outer_shell(self):
        return linear_extrude(height=self.total_height, scale=self.shrink_factor)(
            self.base_form()
        )

    def inner_shell(self):
        return down(0.01)(
            linear_extrude(height=self.height - self.wall, scale=self.shrink_factor)(
                offset(delta=-self.wall)(
                    self.base_form()
                )
            )
        )

    def post_socket(self):
        return \
            up(self.height / 2)(
                cube([8, 8, self.height], center=True)
            )

    def top_slant(self):
        return \
            up(self.height)(
                self.slant_body
            )

    def __call__(self, *args, **kwargs):
        return down(
            self.height - self.wall - self.indent
        )(
            translate([-self.x_offset, -self.y_offset])(
                self.outer_shell()
                -
                self.inner_shell()
                +
                translate([self.x_offset, self.y_offset])(self.post_socket())
                -
                color("orange")(self.top_slant())
            )
        )

    @property
    def base_form(self):
        raise NotImplementedError()

    @property
    def base_length(self):
        raise NotImplementedError()

    @property
    def slant_body(self):
        raise NotImplementedError()


class Square(object):
    """
    Seems that standard key-caps have a 18 × 18 mm dimension.
    This class uses 0.1 mm less for printer inaccuracy.
    """
    def __init__(
        self,
        width=17.9,
        length=17.9,
        height=5,
        top_height=0,
        tilt=0,
        shrink_factor=0.8,
        corner_r=2.5,
        wall=1,
        indent=0.8,
        indent_radius=None,
        squeeze=0.5,
        x_offset=0,
        y_offset=0,
        indent_x_offset=0,
        rotation=0
    ):
        self.width = width
        self.length = length
        self.height = height
        self.top_height = top_height
        self.shrink_factor = shrink_factor
        self.corner_r = corner_r
        self.tilt = tilt
        self.wall = wall
        self.indent = indent
        self.indent_radius = indent_radius
        self.squeeze = squeeze
        self.x_offset = x_offset
        self.y_offset = y_offset
        self.indent_x_offset = indent_x_offset
        self.rotation = rotation

    @property
    def tilt_height(self):
        return deg_tan(self.tilt) * self.length

    @property
    def total_height(self):
        return self.height + self.tilt_height

    @property
    def _rs_w(self):
        return self.width - 2 * self.corner_r

    @property
    def _rs_l(self):
        return self.length - 2 * self.corner_r

    def outer_shell(self):
        return \
            linear_extrude(height=self.total_height, scale=self.shrink_factor)(
                minkowski()(
                    square([self._rs_w, self._rs_l], center=True),
                    circle(self.corner_r, segments=60)
                )
            )

    def inner_shell(self):
        return color("pink")(
            down(0.01)(
                linear_extrude(height=self.height - self.wall - self.indent, scale=self.shrink_factor)(
                    minkowski()(
                        square([self._rs_w - 2*self.wall, self._rs_l - 2*self.wall], center=True),
                        circle(self.corner_r, segments=60)
                    )
                )
            )
        )

    def post_socket(self):
        return \
            up(self.height/2)(
                cube([8, 8, self.height/2], center=True)
            )

    def top_slant(self):
        slant_width = self.width * self.shrink_factor
        slant_height = self.length
        slant_radius = self.indent_radius or slant_width - 1
        slant_offset = deg_tan(self.tilt) * self.length / 2  # the z offset @ 0, 0
        return \
            up(self.height + slant_offset)(
                rotate(self.tilt, [1, 0, 0])(
                    up(slant_height/2)(
                        color("LightSalmon")(cube([2*slant_width, 2*self.length, slant_height], center=True))
                    ),
                    translate([0, self.length, self.squeeze * slant_radius - self.indent])(
                        rotate(90, [1, 0, 0])(
                            scale([1, self.squeeze, 1])(
                                color("SandyBrown")(cylinder(r=slant_radius, h=2*self.length, segments=100))
                            )
                        )
                    )
                )
            )

    def __call__(self, *args, **kwargs):
        return rotate((0, 0, self.rotation))(
            down(self.height - self.wall - self.indent)(
                translate((-self.x_offset, -self.y_offset, 0))(
                    up(self.top_height)(self.outer_shell())
                    -
                    self.inner_shell()
                    +
                    translate((self.x_offset, self.y_offset, 0))(self.post_socket())
                    -
                    right(self.indent_x_offset)(
                        up(self.top_height)(
                            self.top_slant()
                        )
                    )
                )
            )
        )

