from solid import scad_render_to_file
from keycab import shells, interfaces, Cap

cap = Cap(
    shell=shells.Square(
        width=17.5,
        length=17.5,
        height=5.4,
        tilt=0,
        shrink_factor=0.8,
        corner_r=2.5,
        wall=1,
        indent=0.8,
        indent_radius=None,
        squeeze=0.5
    ),
    interface=interfaces.MXInterface(
        height=4
    )
)


def create_triple(name):
    scad_render_to_file(cap(), name + ".scad")
    cap.shell.width = 20
    cap.shell.indent_x_offset = -0.5
    cap.shell.x_offset = -1.25
    scad_render_to_file(cap(), name + "_right.scad")
    cap.shell.indent_x_offset = 0.5
    cap.shell.x_offset = 1.25
    scad_render_to_file(cap(), name + "_left.scad")
    cap.shell.width = 17.5
    cap.shell.indent_x_offset = 0
    cap.shell.x_offset = 0


cap.shell.height = 6
cap.shell.tilt = 8
create_triple("row3")

cap.shell.height = 5.4
cap.shell.tilt = 0
create_triple("row2")

cap.shell.height = 5
cap.shell.tilt = 2
create_triple("row1")

cap.shell.height = 5.5
cap.shell.tilt = 3
cap.shell.y_offset = -1.25
cap.shell.length = 20
create_triple("row0")

cap.shell.height = 8.5
cap.shell.tilt = 13
cap.shell.y_offset = -1.25
cap.shell.length = 20
create_triple("row4")

thumb_cap = Cap(
    shell=shells.Square(
        height=5.4,
        length=26,
        width=17.5
    ),
    interface=interfaces.MXInterface(
        height=5
    )
)

scad_render_to_file(thumb_cap(), "thumb.scad")

thumb_cap.shell.width = 22
thumb_cap.shell.x_offset = 2.25
thumb_cap.shell.indent_x_offset = 0.9

scad_render_to_file(thumb_cap(), "thumb_side.scad")
