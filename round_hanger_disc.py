from solid import (
    scad_render_to_file,
    cylinder
)
from solid.utils import up

segments = 80

d_hole = 5.6
d_base = 10
d_disc_cutout = 10.5
h_disc_cutout = 2
h_base = 12
d_disc = d_disc_cutout + 10 * 0.4
h_disc = 5


hanger =\
    up(0)(
        up(h_disc)(cylinder(
            h=h_base,
            d=d_base,
            segments=segments
        ))
        +
        cylinder(
            h=h_disc,
            d=d_disc,
            segments=segments
        )
        -
        up(-1)(
            cylinder(
                h=d_base + d_disc + 2,
                d=d_hole,
                segments=segments
            )
        )
        -
        up(-1)(
            cylinder(
                h=h_disc_cutout + 1,
                d=d_disc_cutout,
                segments=segments
            )
        )
    )

scad_render_to_file(
    hanger,
    "round_hanger.scad"
)
