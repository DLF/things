from solid import (
    scad_render_to_file,
    circle,
    linear_extrude
)
from solid.utils import up

segments = 80
d_outer = 4.5
d_inner = 2.4
height = 2.2

scad_render_to_file(
    linear_extrude(height)(
        circle(d=d_outer, segments=segments) - circle(d=d_inner, segments=segments)
    ),
    "screw_spacer.scad"
)
