use <tooth_brush_cover.scad>

toothbrush_cover(
    plastic_head_height = 5,
    plastic_head_width = 13.8,
    brush_cube_height = 13.4,
    brush_cube_width = 11.5,
    length = 26,
    wall = 1.2
);
