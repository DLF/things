use <tooth_brush_cover.scad>

toothbrush_cover(
    plastic_head_height = 4.9,
    plastic_head_width = 14.2,
    brush_cube_height = 13,
    brush_cube_width = 12.5,
    length = 28,
    wall = 1
);
