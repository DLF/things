// Cover for Elmex tooth brush
// plastic head: 13 wide, 26 long, 4.9 high
// brush area: 11 wide, 12.5 high
//


module cutaway(h_width, h_height, b_width, b_height, length) {
    color("red")
        translate([-h_width/2, -1, 0])
            cube([h_width, length + 1, h_height]);
    color("yellow")
        translate([-b_width/2, -1, h_height - 0.01])
            cube([b_width, length + 1, b_height]);
}

module outline(h_width, h_height, b_width, b_height, length, wall) {
    translate([-(h_width + 2*wall)/2, 0, -wall])
        cube([h_width + 2*wall, length + wall , h_height + 2*wall]);

    translate([-(b_width + 2*wall)/2, 0 , h_height - wall])
        cube([b_width + 2*wall, length + wall, b_height + 2*wall]);
}

module toothbrush_cover(
    plastic_head_height = 4.95,
    plastic_head_width = 13,
    brush_cube_height = 12.5,
    brush_cube_width = 11,
    length = 26,
    wall = 1
    ) {
    difference() {
        outline(plastic_head_width, plastic_head_height, brush_cube_width, brush_cube_height, length, wall);
        cutaway(plastic_head_width, plastic_head_height, brush_cube_width, brush_cube_height, length);
        //translate([0,0,-10]) cube([50, length*2, 40]);
    }
}

toothbrush_cover();
