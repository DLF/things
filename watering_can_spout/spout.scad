// diameter of the hull at the end, plugged on the can
loose_fit_diameter = 22.7;
// diameter of the hull at the sink end
tight_fit_diameter = 21.4;
// length of the hull part that is slipped over the can spout
hull_length = 25;

// detailed options
hull_extension_length = 3;
sink_diameter = 8;
sink_nick_width = 3;
sink_nick_length = 5;
sink_length = 15;
wall_thickness = 1.2;


function sink_alpha() = acos((sink_nick_width/2) / (sink_diameter/2));

function sink_nick_offset() = sin(sink_alpha()) * sink_diameter/2;

function dimunition_big_inner_diameter() = tight_fit_diameter;
function dimunition_small_inner_diameter() = sink_diameter/2 + sink_nick_width/2 + sink_nick_length + sink_nick_offset();
function dimunition_length() = (dimunition_big_inner_diameter() - dimunition_small_inner_diameter()) / 1;



$fn=80;
/*
 * sink sits on top of z=0
 */
module sink(cutting_die=false) {


    wall = cutting_die ? 0 : wall_thickness;
    length = cutting_die ? sink_length + 2 : sink_length;
    z_offset = cutting_die ? -1 : 0;

    sink_nick_offset = sink_nick_offset();
    total_length = sink_diameter + sink_nick_length / 2 + sink_nick_width / 2 + sink_nick_offset; 



    translate([(-sink_nick_offset - sink_nick_length - sink_nick_width/2)/2 + sink_diameter/4, 0, z_offset]) {

        //main sink pipe
        cylinder(h=length, d=sink_diameter + 2*wall);

        //sink nick
        translate([sink_nick_offset, 0, 0]) { 
            //color("green")
                translate([-wall, -sink_nick_width/2 - wall, 0])
                    cube([sink_nick_length + wall, sink_nick_width + 2*wall, length]);
            //color("blue")
                translate([sink_nick_length, 0, 0])
                    cylinder(h=length, d=sink_nick_width + 2*wall);
        }

    }
}


module diminution() {
    small_inner_diameter = dimunition_small_inner_diameter();
    big_inner_diameter = dimunition_big_inner_diameter();
    outer_length = dimunition_length(); 
    inner_length = outer_length + hull_extension_length;
    difference() {
        union() {
            translate([0,0,-outer_length])
                cylinder(h=outer_length, d1=big_inner_diameter + 2*wall_thickness, d2=small_inner_diameter + 2*wall_thickness);
            translate([0,0,-inner_length])
                cylinder(h=hull_extension_length, d=big_inner_diameter + 2*wall_thickness);
        }
        translate([0,0,-inner_length])
            cylinder(h=inner_length, d1=big_inner_diameter, d2=small_inner_diameter);
    }
    cylinder(h=sink_length/1, d1=small_inner_diameter + 2*wall_thickness, d2=0);
}


module thing() {
    difference() {
        union() {
            sink();
            diminution();
            translate([0, 0, -hull_length-hull_extension_length-dimunition_length()])
                cylinder(h=hull_length,d1=loose_fit_diameter + 2*wall_thickness, d2=tight_fit_diameter + 2*wall_thickness);
        }
        color("red")
            sink(cutting_die=true);
        color("yellow")
            translate([0, 0, -hull_length-hull_extension_length-dimunition_length()-0.1])
                cylinder(h=hull_length + 0.2, d1=loose_fit_diameter, d2=tight_fit_diameter);
        color("magenta")
            translate([0, 0, -0.01])
                cylinder(h=sink_length/1.2, d1=dimunition_small_inner_diameter(), d2=0);
    }
}

thing();

/*
difference() {
sink();
sink(true);
}
cylinder(h=1,d=dimunition_small_inner_diameter());
*/
